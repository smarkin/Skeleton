"""Packaging logic for skeleton."""

import setuptools
import skeleton
from setuptools import find_packages

# sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'src'))


with open('README.txt') as f:
    readme = f.read()

VERSION = skeleton.__version__
AUTHOR = skeleton.__author__
PACKAGE = skeleton.__name__

tests_require = [
    'mock >= 2.0.0',
    'pytest>=2.7.3',
    'pytest-html',
]

install_requires = [
    "six>=1.9.0",
    "click>=6.7",
    "knack",
]

classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Environment :: Console",
    "Framework :: Skeleton",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.5",
    "Programming Language :: Python :: 3.6",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Topic :: Software Development :: Quality Assurance",
]

description = """
Python script to post message to the slack channel
and generate message by templates.
"""

setuptools.setup(
    name=PACKAGE,
    version=VERSION,
    description=description,
    long_description=readme,
    author=AUTHOR,
    author_email="smarkin@vrconcept.net",
    url='http://vrconcept.net',
    license='GPL',
    classifiers=classifiers,
    tests_require=tests_require,
    install_requires=install_requires,
    setup_requires=[
        'pytest-runner'
    ],
    packages=find_packages(
        where=".",
        exclude=[
            'contrib',
            'docs',
            'tests',
            'build',
            'dist']),
    entry_points={
        'console_scripts': [
            '{package}={package}.core.cli:main'.format(package=PACKAGE),
        ],
    },

)
