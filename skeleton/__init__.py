"""Top-level module for skeletom.

This module

- initializes logging for the command-line tool
- tracks the version of the package
- provides a way to configure logging for the command-line tool

.. autofunction:: skeleton.configure_logging

"""

import logging
import sys
import uuid

try:
    uuid.uuid1()
except ValueError:
    uuid.uuid1 = uuid.uuid4


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


__all__ = ['HelloWorld', 'cli']
__author__ = 'Markin Sergey'
__version__ = '0.3.15-dev.9'
__version_info__ = tuple(int(i) for i in __version__.split('.') if i.isdigit())


# There is nothing lower than logging.DEBUG (10) in the logging library,
# but we want an extra level to avoid being too verbose when using -vv.
_EXTRA_VERBOSE = 5
logging.addLevelName(_EXTRA_VERBOSE, 'VERBOSE')

# Logging messages which are less severe than level will be ignored
# CRITICAL = 50
# FATAL = CRITICAL
# ERROR = 40
# WARNING = 30
# WARN = WARNING
# INFO = 20
# DEBUG = 10
# NOTSET = 0

_VERBOSITY_TO_LOG_LEVEL = {
    # output error information
    0: logging.ERROR,
    # output warnings info
    1: logging.WARNING,
    # output info
    2: logging.INFO,
    # output debugging information
    3: logging.DEBUG,
}

# verbose level
# -1 nothing
# 0 ERROR
# 1 WARNING ERROR
# 2 INFO WARNING ERROR
# 3 DEBIG INFO WARNING ERROR

LOG_FORMAT = ('%(name)-25s %(processName)-11s {%(filename)s:%(lineno)d} %(relativeCreated)6d '
              '%(levelname)-8s %(message)s')


def configure_logging(verbosity: int, filename: str=None,
                      logformat=LOG_FORMAT):
    """Configure logging for acrcli.

    :param int verbosity:
        How verbose to be in logging information.
    :param str filename:
        Name of the file to append log information to.
        If ``None`` this will log to ``sys.stderr``.
        If the name is "stdout" or "stderr" this will log to the appropriate
        stream.
    """
    if verbosity < 0:
        return
    if verbosity > 3:
        verbosity = 3

    log_level = _VERBOSITY_TO_LOG_LEVEL[verbosity]

    if not filename or filename in ('stderr', 'stdout'):
        fileobj = getattr(sys, filename or 'stderr')
        handler_cls = logging.StreamHandler
    else:
        fileobj = filename
        handler_cls = logging.FileHandler

    handler = handler_cls(fileobj)
    handler.setFormatter(logging.Formatter(logformat))
    log.addHandler(handler)
    log.setLevel(log_level)
    log.debug('Added a %s logging handler to logger root at %s',
              filename, __name__)
