"""Module allowing for ``python -m skeleton ...``."""
from skeleton.core import cli  # pragma: no cover

if __name__ == '__main__':
    cli.main()
