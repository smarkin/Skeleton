"""Command-line implementation of skeleton."""

import click

import skeleton
from skeleton.core.HelloWorld import HelloWorld
from skeleton.defaults import TEXT


def main():  # pragma: no cover
    """The enter point."""
    return cli(obj={})


def print_version(ctx, param, value):
    """Printed version into console.

    :param ctx: test context
    :param param: unused
    :param value: unused
    :return:
    """
    if not value or ctx.resilient_parsing:
        return
    print('Version:', skeleton.__version__)
    ctx.exit()


@click.group()
@click.option('--version', is_flag=True, callback=print_version,
              expose_value=False, is_eager=True)
@click.pass_context
def cli(ctx):
    """
    The enter point of the module.

    :param ctx: test context
    :return:
    """
    hello = HelloWorld()
    ctx.obj['hello'] = hello


@cli.command()
@click.option('--msg', default=TEXT, type=str, help='print your message')
@click.pass_context
def say(ctx, msg):
    """
    Simple method.

    :param ctx: test context
    :param msg: text that you want to print.
    :return:
    """
    hello = ctx.obj['hello']
    hello.print_msg(msg)
    ctx.exit(code=0)
