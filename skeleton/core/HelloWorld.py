"""Class HelloWorld."""
import skeleton

log = skeleton.log


class HelloWorld(object):
    """Demo class."""

    def __init__(self, *args, **kwargs):
        """
        If there are decorator arguments,
        the function to be decorated is not passed to the constructor!
        """

    def print_msg(self, msg):
        """
        Simple method of the class.

        :param msg: text that will be print
        :return:
        """
        log.info(msg)
