"""Module with tests."""

from skeleton.core.HelloWorld import HelloWorld


class TestCore:
    """Simple class for tests."""

    def setup_class(self):
        """Setup class."""

        print("setup class: ")
        self.hello = HelloWorld()

    def setup_method(self, method):
        """Setup method."""
        print("method setup : ", method.__name__)

    def setup(self):
        """Basic setup"""
        print("basic setup start test")

    def teardown(self):
        """Basic teardown into class."""
        print("basic teardown into class")

    def teardown_method(self, method):
        """Method teardown."""
        print("method teardown :", method.__name__)

    def teardown_class(self):
        """Teardown class."""
        print("cleaning...")

    def test_something(self):
        """Simple test."""
        self.hello.print_msg("Hello, World")
        assert True
