"""Configuration tests."""


def pytest_addoption(parser):
    """
    Add options

    :param parser:
    :return:
    """
    parser.addoption(
        "--number",
        action="store",
        type=int,
        default=1,
        help="number repetitions tests"
    )


def pytest_generate_tests(metafunc):
    """Generate tests."""
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    number_value = metafunc.config.getoption('number')

    if 'number' in metafunc.fixturenames and number_value is not None:
        metafunc.parametrize("number", [number_value])
