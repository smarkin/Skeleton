"""Module with tests."""
import pytest


@pytest.mark.parametrize("y", [2, 3])
def test_args(number, y):
    """Simple test with arguments."""
    print(y)
    assert number == 1
