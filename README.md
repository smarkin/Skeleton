Template the project for creation of python tools.

Install pre-commit

> pip install pre-commit


Configure pre-commit

> .pre-commit-config.yaml


Run pre-commit install to install pre-commit into your git hooks.

> pre-commit install -c .tools/.pre-commit-config.yaml

Attach deploy key to the project